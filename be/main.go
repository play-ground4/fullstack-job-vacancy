package main

import (
	"fmt"
	"golang-job-vacancy-api/controllers"
	"golang-job-vacancy-api/database"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {
	LoadAppConfig()
	// Initialize Database
	database.Connect(AppConfig.ConnectionString)
	database.Migrate()

	// setup cors
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET"},
	})

	// Initialize the router
	router := mux.NewRouter().StrictSlash(true)
	// Register Routes
	EmployeeTypeRoutes(router)
	PositionLevelRoutes(router)
	JobVacancyRoutes(router)
	// Start the server
	log.Println(fmt.Sprintf("Starting Server on port %s", AppConfig.Port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", AppConfig.Port), c.Handler(router)))
}

func EmployeeTypeRoutes(router *mux.Router) {
	router.HandleFunc("/api/employee-type", controllers.GetAllEmployeeType).Methods("GET")
	router.HandleFunc("/api/employee-type/{id}", controllers.GetEmployeeTypeById).Methods("GET")
	router.HandleFunc("/api/employee-type", controllers.CreateEmployeeType).Methods("POST")
	router.HandleFunc("/api/employee-type/{id}", controllers.DeleteEmployeeType).Methods("DELETE")
}

func PositionLevelRoutes(router *mux.Router) {
	router.HandleFunc("/api/position-level", controllers.GetAllPositionLevel).Methods("GET")
	router.HandleFunc("/api/position-level/{id}", controllers.GetPositionLevelById).Methods("GET")
	router.HandleFunc("/api/position-level", controllers.CreatePositionLevel).Methods("POST")
	router.HandleFunc("/api/position-level/{id}", controllers.DeletePositionLevel).Methods("DELETE")
}

func JobVacancyRoutes(router *mux.Router) {
	router.HandleFunc("/api/job-vacancy", controllers.GetAllJobVacancy).Methods("GET")
	router.HandleFunc("/api/job-vacancy/{id}", controllers.GetJobVacancyById).Methods("GET")
	router.HandleFunc("/api/job-vacancy", controllers.CreateJobVacancy).Methods("POST")
	router.HandleFunc("/api/job-vacancy/{id}", controllers.DeleteJobVacancy).Methods("DELETE")
}
