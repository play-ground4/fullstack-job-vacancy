### Project prepararion and setup

- Installed and Running MySQL
- Installed and Running Go
- Install dependencies by running `go mod dowload`
- Set your connection string and app port on `config.json` file
- All set, run your app by `go run .`
- By default, API host should looks like http://localhost:8001