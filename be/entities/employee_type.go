package entities

type EmployeeType struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
