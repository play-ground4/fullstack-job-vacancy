package entities

type JobVacancy struct {
	ID                   uint   `json:"id"`
	Title                string `json:"title"`
	Description          string `json:"description"`
	MinimumQualification string `json:"minimum_qualification"`
	MinimumExperience    string `json:"minimum_experience"`
	Skills               string `json:"skills"`
	Benefit              string `json:"benefit"`
	EmployeeTypeId       uint   `json:"employee_type_id"`
	PositionLevelId      uint   `json:"position_level_id"`
}
