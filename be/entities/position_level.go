package entities

type PositionLevel struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
