package controllers

import (
	"encoding/json"
	"golang-job-vacancy-api/database"
	"golang-job-vacancy-api/entities"
	"net/http"

	"github.com/gorilla/mux"
)

func GetAllEmployeeType(w http.ResponseWriter, r *http.Request) {
	var datas []entities.EmployeeType
	database.Instance.Find(&datas)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(datas)
}

func CreateEmployeeType(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var data entities.EmployeeType

	json.NewDecoder(r.Body).Decode(&data)
	database.Instance.Create(&data)
	json.NewEncoder(w).Encode("Created Successfully!")
}

func checkIfEmployeeTypeExists(EmployeeTypeIdentity string) bool {
	var data entities.EmployeeType

	database.Instance.First(&data, EmployeeTypeIdentity)
	if data.ID == 0 {
		return false
	}

	return true
}

func GetEmployeeTypeById(w http.ResponseWriter, r *http.Request) {
	employeeTypeId := mux.Vars(r)["id"]

	if checkIfEmployeeTypeExists(employeeTypeId) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employee Type Not Found!")
		return
	}

	var data entities.EmployeeType
	database.Instance.First(&data, employeeTypeId)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func DeleteEmployeeType(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	employeeTypeId := mux.Vars(r)["id"]

	if checkIfEmployeeTypeExists(employeeTypeId) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employee Type Not Found!")
		return
	}

	var data entities.EmployeeType
	database.Instance.Delete(&data, employeeTypeId)
	json.NewEncoder(w).Encode("Deleted Successfully!")
}
