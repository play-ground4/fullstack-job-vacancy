package controllers

import (
	"encoding/json"
	"golang-job-vacancy-api/database"
	"golang-job-vacancy-api/entities"
	"net/http"

	"github.com/gorilla/mux"
)

func GetAllPositionLevel(w http.ResponseWriter, r *http.Request) {
	var datas []entities.PositionLevel
	database.Instance.Find(&datas)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(datas)
}

func CreatePositionLevel(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var data entities.PositionLevel

	json.NewDecoder(r.Body).Decode(&data)
	database.Instance.Create(&data)
	json.NewEncoder(w).Encode("Created Successfully!")
}

func checkIfPositionLevelExists(PositionLevelIdentity string) bool {
	var data entities.PositionLevel

	database.Instance.First(&data, PositionLevelIdentity)
	if data.ID == 0 {
		return false
	}

	return true
}

func GetPositionLevelById(w http.ResponseWriter, r *http.Request) {
	positionLevelId := mux.Vars(r)["id"]

	if checkIfPositionLevelExists(positionLevelId) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Position Level Not Found!")
		return
	}

	var data entities.PositionLevel
	database.Instance.First(&data, positionLevelId)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func DeletePositionLevel(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	positionLevelId := mux.Vars(r)["id"]

	if checkIfPositionLevelExists(positionLevelId) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Position Level Not Found!")
		return
	}

	var data entities.PositionLevel
	database.Instance.Delete(&data, positionLevelId)
	json.NewEncoder(w).Encode("Deleted Successfully!")
}
