package controllers

import (
	"encoding/json"
	"golang-job-vacancy-api/database"
	"golang-job-vacancy-api/entities"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func GetAllJobVacancy(w http.ResponseWriter, r *http.Request) {
	var datas []entities.JobVacancy
	database.Instance.Find(&datas)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(datas)
}

func CreateJobVacancy(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var data entities.JobVacancy

	json.NewDecoder(r.Body).Decode(&data)

	if checkIfEmployeeTypeExists(strconv.FormatUint(uint64(data.EmployeeTypeId), 10)) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employe Type ID Not Found!")
		return
	}
	if checkIfPositionLevelExists(strconv.FormatUint(uint64(data.PositionLevelId), 10)) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Position Level ID Not Found!")
		return
	}

	database.Instance.Create(&data)
	json.NewEncoder(w).Encode("Created Successfully!")
}

func checkIfJobVacancyExists(jobVacancyIdentity string) bool {
	var data entities.JobVacancy

	database.Instance.First(&data, jobVacancyIdentity)
	if data.ID == 0 {
		return false
	}

	return true
}

func GetJobVacancyById(w http.ResponseWriter, r *http.Request) {
	jobVacancyId := mux.Vars(r)["id"]

	if checkIfJobVacancyExists(jobVacancyId) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Job Vacancy Not Found!")
		return
	}

	var data entities.JobVacancy
	database.Instance.First(&data, jobVacancyId)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func DeleteJobVacancy(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	jobVacancyId := mux.Vars(r)["id"]

	if checkIfJobVacancyExists(jobVacancyId) == false {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Job Vacancy Not Found!")
		return
	}

	var product entities.JobVacancy
	database.Instance.Delete(&product, jobVacancyId)
	json.NewEncoder(w).Encode("Deleted Successfully!")
}
