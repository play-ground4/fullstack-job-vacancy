### Project prepararion and setup

- Installed and Running Nodejs minimum version 16.04
- Installed and Running npm
- Install dependencies by running `npm install`
- Set your api host on `src/api/config.js` file
- All set, run your app by `npm run dev`
- By default, API host should looks like http://localhost:3000