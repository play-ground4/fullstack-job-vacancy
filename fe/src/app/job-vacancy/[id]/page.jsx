"use client";

import { HttpRequest } from "@/api/job-vacancy-api";
import Alert from "@/components/common/Alert";
import Header from "@/components/common/Header";
import Loading from "@/components/common/Loading";
import React, { useEffect, useState } from "react";

import styles from "./page.module.css";

export default ({ params }) => {
  const { id } = params;

  const [job, setJob] = useState(null);
  const [jobLoding, setJobLoading] = useState(false);

  useEffect(() => {
    const getJobDetail = async () => {
      setJobLoading(true);
      const { error, data } = await HttpRequest.getJobVacancyById(id);
      setJobLoading(false);

      if (!error) setJob(data);
    };

    getJobDetail();

    return () => {
      setJob(null);
    };
  }, []);

  return (
    <>
      <nav>
        <div className="mb-2">
          <Header />
        </div>
      </nav>

      <main>
        {jobLoding && <Loading />}
        {!jobLoding && !job && <Alert message="Data Tidak Ditemukan" />}
        {!jobLoding && job && (
          <>
            <div className={styles.job__header}>
              <h2>{job.title}</h2>
              <button>Apply Now</button>
            </div>

            <div className={styles.job__section}>
              <h3>Job Description</h3>
              <div>{job.description}</div>
            </div>

            <div className={styles.job__section}>
              <h3>Job Minimum Qualification</h3>
              <div>{job.minimum_qualification}</div>
            </div>

            <div className={styles.job__section}>
              <h3>Minimum Experience</h3>
              <div>{job.minimum_experience}</div>
            </div>

            <div className={styles.job__section}>
              <h3>Skills</h3>
              <div>{job.skills}</div>
            </div>

            <div className={styles.job__section}>
              <h3>Benefit</h3>
              <div>{job.benefit}</div>
            </div>
          </>
        )}
      </main>
    </>
  );
};
