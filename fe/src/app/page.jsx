"use client";

import React, { useEffect, useState } from "react";

import Header from "@/components/common/Header";
import SectionTitle from "@/components/text/SectionTitle";
import Checkbox from "@/components/form/Checkbox";
import JobVacancyItemCard from "@/components/card/JobVacancyItemCard";

import styles from "./page.module.css";
import { HttpRequest } from "@/api/job-vacancy-api";
import Loading from "@/components/common/Loading";
import Alert from "@/components/common/Alert";

export default () => {
  const [employeeTypes, setEmployeeTypes] = useState([]);
  const [positionLevels, setPositionLevels] = useState([]);
  const [jobVacancies, setJobVacancies] = useState([]);

  const [employeeTypeLoading, setEmployeeTypeLoading] = useState(false);
  const [positionLevelLoading, setPositionLevelLoading] = useState(false);
  const [jobVacancyLoading, setJobVacancyLoading] = useState(false);

  const [filterEmployeeType, setFilterEmployeeType] = useState([]);
  const [filterPositionLevel, setFilterPositionLevel] = useState([]);

  function filter(data) {
    console.log(filterEmployeeType, filterPositionLevel);
    if(filterEmployeeType.length == 0 && filterPositionLevel.length == 0) return data;

    return data.filter((item) => {
      console.log(item.employee_type_id, item.position_level_id);
      const hasEmployeeType = filterEmployeeType.length == 0 || filterEmployeeType.includes(item.employee_type_id.toString());
      const hasPositionLevel = filterPositionLevel.length == 0 || filterPositionLevel.includes(item.position_level_id.toString());
      console.log(hasEmployeeType, hasPositionLevel);

      return hasEmployeeType && hasPositionLevel;
    });
  }

  function onEmployeeTypeChecked(e){
    const { checked } = e.target;
    const targetId = e.target.getAttribute('data-id');

    if(checked){
      setFilterEmployeeType((old) => [
        ...old,
        targetId
      ])
    }else{
      setFilterEmployeeType((old) => [
        ...old.filter((item) => item !== targetId)
      ])
    }
  }

  function onPositionLevelChecked(e){
    const { checked } = e.target;
    const targetId = e.target.getAttribute('data-id');

    if(checked){
      setFilterPositionLevel((old) => [
        ...old,
        targetId
      ])
    }else{
      setFilterPositionLevel((old) => [
        ...old.filter((item) => item !== targetId)
      ])
    }
  }

  useEffect(() => {
    const getEmployeeType = async () => {
      setEmployeeTypeLoading(true);
      const { error, data } = await HttpRequest.getAllEmployeeType();
      setEmployeeTypeLoading(false);

      if (!error) setEmployeeTypes(data);
    };
    const getPositionLevel = async () => {
      setPositionLevelLoading(true);
      const { error, data } = await HttpRequest.getAllPositionLevel();
      setPositionLevelLoading(false);

      if (!error) setPositionLevels(data);
    };
    const getJobVacancy = async () => {
      setJobVacancyLoading(true);
      const { error, data } = await HttpRequest.getAllJobVacancies();
      setJobVacancyLoading(false);

      if (!error) setJobVacancies(data);
    };

    getEmployeeType();
    getPositionLevel();
    getJobVacancy();

    return () => {
      setEmployeeTypes([]);
      setPositionLevels([]);
      setJobVacancies([]);
    };
  }, []);

  return (
    <>
      <nav>
        <div className="mb-2">
          <Header />
        </div>

        <div className="mb-2">
          <div className="mb-1">
            <SectionTitle title="Employment Type" />
          </div>

          { employeeTypeLoading && <Loading/> }
          { !employeeTypeLoading && employeeTypes.length == 0 && <Alert message="Belum Ada Data"/> }
          { !employeeTypeLoading && employeeTypes.length > 0 && employeeTypes.map((item) => {
            return <div className="mb-1">
            <Checkbox dataId={item.id} label={item.name} onChangeHandler={onEmployeeTypeChecked} />
          </div>
          }) }
        </div>

        <div className="mb-2">
          <div className="mb-1">
            <SectionTitle title="Position Level" />
          </div>

          { positionLevelLoading && <Loading/> }
          { !positionLevelLoading && positionLevels.length == 0 && <Alert message="Belum Ada Data"/> }
          { !positionLevelLoading && positionLevels.length > 0 && positionLevels.map((item) => {
            return <div className="mb-1">
            <Checkbox dataId={item.id} label={item.name} onChangeHandler={onPositionLevelChecked} />
          </div>
          }) }
        </div>
      </nav>

      <main>
        <div className={styles.job__vacancy__wrap}>
        { jobVacancyLoading && <Loading/> }
          { !jobVacancyLoading && jobVacancies.length == 0 && <Alert message="Belum Ada Data"/> }
          { !jobVacancyLoading && jobVacancies.length > 0 && filter(jobVacancies).length == 0 && <Alert message="Data Tidak Ditemukan"/> }
          { !jobVacancyLoading && jobVacancies.length > 0 && filter(jobVacancies).map((item) => {
            return (
              <JobVacancyItemCard
                jobVacancy={item}
              />
            );
          })}
        </div>
      </main>
    </>
  );
};
