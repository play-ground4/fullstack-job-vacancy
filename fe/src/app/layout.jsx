import './globals.css';

export const metadata = {
  title: 'Job Vacancy',
  description: 'Find your best fit job here',
}
 
export default function RootLayout({ children }) {
 return (
    <html lang="en">
      <body>
        {children}
      </body>
    </html>
  )
}
