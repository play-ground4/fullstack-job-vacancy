import React from "react";

import styles from './section_title.module.css';

export default ({
    title
}) => {
    return <>
    <h3 className={styles.section__title}>{title}</h3>
    </>
}