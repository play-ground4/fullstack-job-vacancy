import React from "react";

import styles from './job_vacancy_item_card.module.css';
import Link from "next/link";

export default ({
    jobVacancy
}) => {
    return <>
    <div className={styles.card__wrap}>
        <h3 className={styles.card__title}>{jobVacancy.title}</h3>
        <div className={styles.card__description}>{jobVacancy.description}</div>

        <div className={styles.card__action}>
            <Link className={styles.card__action__link} href={`/job-vacancy/${jobVacancy.id}`}>View Details</Link>
            <button className={styles.card__action__button}>Apply</button>
        </div>
    </div>
    </>
}