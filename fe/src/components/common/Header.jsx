import React from "react";

import styles from './header.module.css';
import Link from "next/link";

export default () => {
  return (
    <>
    <Link href="/">
      <h1 className={styles.web__title}>
        <span>Join</span> Our Team
      </h1>
    </Link>
    </>
  );
};
