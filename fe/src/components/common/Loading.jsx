import React from "react";

import styles from './loading.module.css';

export default function () {
    return <>
        <div className={styles.loading__text}>loading ...</div>
    </>
}