import React from "react";

import styles from './alert.module.css';

export default function ({
    message = 'Data Tidak ditemukan'
}) {
    return <>
        <div className={styles.alert__text}>{message}</div>
    </>
}