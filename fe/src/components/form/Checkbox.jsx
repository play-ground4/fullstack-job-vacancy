'use client';

import React from "react";

import styles from './checkbox.module.css'

export default ({
    dataId,
    label,
    onChangeHandler
}) => {
    return <>
    <div className={styles.checkbox__wrap}>
        <input data-id={dataId} onChange={onChangeHandler} type="checkbox" /> <span>{label}</span>
    </div>
    </>
}