import { fetchUrl } from "./http";

const requestWrapper = async (request) => {
    try{
        const response = await request;
        return {
            error: response.code > 300,
            data: await response.json()
        };
    }catch(e){
        return {
            error: true,
            message: e.message
        }
    }
}

const HttpRequest = {
    async getAllEmployeeType(){
        return await requestWrapper(fetchUrl('/employee-type', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }));
    },

    async getAllPositionLevel(){
        return await requestWrapper(fetchUrl('/position-level', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }));
    },

    async getAllJobVacancies(){
        return await requestWrapper(fetchUrl('/job-vacancy', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }));
    },

    async getJobVacancyById(vacancyId){
        return await requestWrapper(fetchUrl(`/job-vacancy/${vacancyId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }));
    },
}

export {
    HttpRequest
}