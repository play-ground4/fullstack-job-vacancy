import { BASE_URL } from "./config";

const fetchUrl = (endpoint, options) => {
  return fetch(`${BASE_URL}${endpoint}`, {
    ...options,
    headers: {
      ...options.headers,
    },
  });
};

export {
    fetchUrl
}